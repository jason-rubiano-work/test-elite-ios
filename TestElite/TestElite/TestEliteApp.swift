//
//  testEliteApp.swift
//  testElite
//
//  Created by NECSOFT on 7/02/24.
//

import SwiftUI

@main
struct testEliteApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
