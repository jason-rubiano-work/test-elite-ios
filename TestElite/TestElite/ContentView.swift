//
//  ContentView.swift
//  testElite
//
//  Created by NECSOFT on 7/02/24.
//

import SwiftUI

struct ContentView: View {
    
    @StateObject var viewModel: ImagePickerVM = ImagePickerVM()
    @Namespace var animation
    
    var body: some View {
        
        NavigationView {
            
            ZStack {
             
                VStack {
                    
                    HStack(spacing: 22) {
                        
                        Button(action: {
                            
                            viewModel.moreNumbreGrid()
                            
                        }) {
                            
                            Image(systemName: "rectangle.grid.2x2")
                                .foregroundColor(viewModel.columns > 1 ? Color.blue : Color.gray)
                                .imageScale(.large)
                        }
                        
                        Button(action: {
                            
                            viewModel.lessNumbreGrid()
                            
                        }) {
                            
                            Image(systemName: "rectangle.grid.1x2")
                                .foregroundColor(viewModel.columns == 1 ? Color.blue : Color.gray)
                                .imageScale(.large)
                        }
                        
                    }.padding(.bottom, 10)
                    
                    StaggeredGrid(columns: viewModel.columns, list: viewModel.imageData, content: { item in
                        ImageCard(image: item, select: $viewModel.imageSelect)
                            .matchedGeometryEffect(id: item.id, in: animation)
                    })
                    
                }.padding([.top, .horizontal], 16)
                .padding(.bottom, 22)
                .sheet(isPresented: $viewModel.showingImagePicker) {
                    ImagePickerView(images: $viewModel.imageData)
                        .ignoresSafeArea()
                }
                
                if let imageSelect = viewModel.imageSelect {
                    
                    ImageDetailsCard(imageSelect: imageSelect, select: $viewModel.imageSelect)
                }
                
            }.navigationTitle("Mis fotos")
                .toolbar {
                    
                    ToolbarItem(placement: .navigationBarLeading) {
                        Button(action: {
                            viewModel.saveImages()
                        }) {
                            Text("Guardar")
                        }.disabled(viewModel.imageData.isEmpty)
                    }
                    
                    ToolbarItem(placement: .navigationBarTrailing) {
                        Button(action: {
                            viewModel.changeShowingImagePicker()
                        }) {
                            Text("Agregar")
                        }
                    }
                }
            .background(Color.white)
            .edgesIgnoringSafeArea(.bottom)
            .onAppear() {
                Commons().requestPhotoLibraryAccess()
            }
            .animation(.easeInOut, value: viewModel.columns)
        }
    }
}

#Preview {
    ContentView()
}
