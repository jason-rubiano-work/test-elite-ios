//
//  getDate.swift
//  testElite
//
//  Created by NECSOFT on 7/02/24.
//

import Foundation
import Photos

class Commons {
    
    func getDate() -> String {
        
        let currentDate = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
        return dateFormatter.string(from: currentDate)
    }
    
    func requestPhotoLibraryAccess() {
        let status = PHPhotoLibrary.authorizationStatus()

        switch status {
        case .notDetermined:
          PHPhotoLibrary.requestAuthorization { newStatus in
            if newStatus == .authorized {
              print("Access granted.")
            } else {
              print("Access denied.")
            }
          }
        case .restricted, .denied:
          print("Access denied or restricted.")
        case .authorized:
          print("Access already granted.")
        case .limited:
          print("Access limited.")
        @unknown default:
          print("Unknown authorization status.")
        }
      }
}
