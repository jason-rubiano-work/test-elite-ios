//
//  StaggeredGrid.swift
//  testElite
//
//  Created by NECSOFT on 8/02/24.
//

import SwiftUI

struct StaggeredGrid<Content: View, T: Identifiable>: View where T: Hashable {
    
    var content: (T) -> Content
    var list: [T]
    
    var showsIndecators: Bool
    var spacing: CGFloat
    
    var columns: Int
    
    init(columns: Int, showsIndecators: Bool = false, spacing: CGFloat = 10, list: [T], @ViewBuilder content: @escaping (T) -> Content) {
        self.content = content
        self.list = list
        self.spacing = spacing
        self.showsIndecators = showsIndecators
        self.columns = columns
    }
    
    func setUpList() -> [[T]] {
        var gridArray: [[T]] = Array(repeating: [], count: columns)
        
        var currentIndex: Int = 0
        
        for object in list {
            gridArray[currentIndex].append(object)
            
            if currentIndex == (columns - 1) {
                currentIndex = 0
            } else {
                currentIndex += 1
            }
        }
        
        return gridArray
    }
    
    var body: some View {
        
        ScrollView(.vertical, showsIndicators: showsIndecators) {
            
            HStack(alignment: .top) {
                
                ForEach(setUpList(), id: \.self) { columsData in
                    
                    LazyVStack(spacing: spacing) {
                     
                        ForEach(columsData) { object in
                         
                            content(object)
                        }
                    }
                }
            }
        }
    }
}

/*#Preview {
    StaggeredGrid()
}*/
