//
//  ImageModel.swift
//  testElite
//
//  Created by NECSOFT on 7/02/24.
//

import Foundation
import SwiftUI
import CoreLocation

struct ImageModel: Codable, Hashable, Identifiable {
    var id: String?
    var url: String?
    var name: String?
    var dateAddition: String?
}
