//
//  ImagePickerVM.swift
//  testElite
//
//  Created by NECSOFT on 7/02/24.
//

import Foundation

class ImagePickerVM: ObservableObject {
    
    @Published var showingImagePicker = false
    @Published var imageData: [ImageModel] = []
    @Published var imageSelect: ImageModel?
    @Published var columns: Int = 2
    
    init() {
        getData()
    }
    
    func changeShowingImagePicker() {
        showingImagePicker.toggle()
    }
    
    func lessNumbreGrid() {
        
        columns = max(columns - 1, 1)
    }
    
    func moreNumbreGrid() {
        columns += 1
    }
    
    func getData() {
        
        guard let savedData = UserDefaults.standard.data(forKey: "imagesFile") else {return}
        
         do {
             
             let decoder = JSONDecoder()
             let loadedImages = try decoder.decode([ImageModel].self, from: savedData)
             
             if !loadedImages.isEmpty {
                 DispatchQueue.main.async {
                     self.imageData = loadedImages
                 }
             }
             
         } catch {
             print("Error decoding data: \(error)")
         }
    }
    
    func saveImages() {
        
        //DispatchQueue.main.async {
         
            do {
                let encoder = JSONEncoder()
                let encodedImages = try encoder.encode(self.imageData)
                
                UserDefaults.standard.set(encodedImages, forKey: "imagesFile")
                print("JASSS OKKK")
            } catch {
                print("Error encoding data: \(error)")
            }
        //}
    }
    func deleteImages() {
        
        DispatchQueue.main.async {
            self.imageData.removeAll()
        }
        
        UserDefaults.standard.removeObject(forKey: "imagesFile")
    }
}
