//
//  ImagePickerView.swift
//  testElite
//
//  Created by NECSOFT on 7/02/24.
//

import PhotosUI
import SwiftUI
import Foundation

struct ImagePickerView: UIViewControllerRepresentable {
    @Binding var images: [ImageModel]

    func makeUIViewController(context: Context) -> PHPickerViewController {
        var config = PHPickerConfiguration()
        config.filter = .images
        config.selectionLimit = 0
        let picker = PHPickerViewController(configuration: config)
        picker.modalPresentationStyle = .overFullScreen
        picker.delegate = context.coordinator
        return picker
    }

    func updateUIViewController(_ uiViewController: PHPickerViewController, context: Context) {

    }

    func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }

    class Coordinator: NSObject, PHPickerViewControllerDelegate {
        let parent: ImagePickerView

        init(_ parent: ImagePickerView) {
            self.parent = parent
        }

        func picker(_ picker: PHPickerViewController, didFinishPicking results: [PHPickerResult]) {
            picker.dismiss(animated: true)
            
            /*for result in results {
                result.itemProvider.loadItem(forTypeIdentifier: UTType.image.identifier, options: nil) { item, _ in
                    if let url = item as? URL {
                        let imageModel = ImageModel(id: UUID().uuidString, url: url, name: url.lastPathComponent, dateAddition: Commons().getDate())
                        
                        DispatchQueue.main.async {
                            self.parent.images.append(imageModel)
                        }
                    }
                }
            }*/
            
            for result in results {
                
                let provider = result.itemProvider
                
                if provider.canLoadObject(ofClass: UIImage.self) {
                    provider.loadObject(ofClass: UIImage.self) { image, error in
                        if let image = image as? UIImage {
                            
                            let filename = UUID().uuidString + ".png"
                            if let imageUrl = self.saveImage(image, withName: filename) {
                             
                                let imageModel = ImageModel(id: UUID().uuidString, url: imageUrl.absoluteString, name: filename, dateAddition: Commons().getDate())
                                 
                                DispatchQueue.main.async {
                                    self.parent.images.append(imageModel)
                                }
                            }
                        }
                    }
                }
            }
        }
        
        func saveImage(_ image: UIImage, withName name: String) -> URL? {
            guard let data = image.pngData() else { return nil }
            do {
                let fileUrl = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false).appendingPathComponent(name)
                try data.write(to: fileUrl)
                return fileUrl
            } catch {
                print("Error saving image: \(error.localizedDescription)")
                return nil
            }
        }
    }
}

#Preview {
    ImagePickerView(images: .constant([]))
}
