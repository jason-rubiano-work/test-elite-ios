//
//  ImageDetailsCard.swift
//  testElite
//
//  Created by NECSOFT on 7/02/24.
//

import SwiftUI
import SDWebImageSwiftUI

struct ImageDetailsCard: View {
    
    var imageSelect: ImageModel
    @Binding var select: ImageModel?
    
    var body: some View {
        
        ZStack {
            
            VStack(spacing: 16) {
                
                HStack {
                    
                    Spacer()
                    
                    Button(action: {
                        
                        select = nil
                        
                    }) {
                        
                        Image(systemName: "xmark")
                            .foregroundColor(Color.black)
                            .imageScale(.medium)
                    }
                }
                
                WebImage(url: URL(string: imageSelect.url ?? ""))
                    .resizable()
                    .scaledToFit()
                    .cornerRadius(10)
                
                VStack(spacing: 0) {
                 
                    Text("Fecha:\n").bold() +
                    
                    Text(imageSelect.dateAddition ?? "")
                    
                }.frame(maxWidth: .infinity, alignment: .leading)
                
                VStack(spacing: 0) {
                 
                    Text("Nombre:\n").bold() +
                    
                    Text(imageSelect.name ?? "")
                    
                }.frame(maxWidth: .infinity, alignment: .leading)
                
                
            }.padding(.vertical, 16)
                .padding(.horizontal, 22)
                .frame(maxWidth: .infinity)
                .background(Color.white)
                .cornerRadius(16)
                .padding(22)
            
        }.frame(maxWidth: .infinity, maxHeight: .infinity)
            .background(Color.black.opacity(0.3).edgesIgnoringSafeArea(.bottom))
    }
}

#Preview {
    ImageDetailsCard(imageSelect: ImageModel(), select: .constant(nil))
}
