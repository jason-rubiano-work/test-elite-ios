//
//  ImageCard.swift
//  testElite
//
//  Created by NECSOFT on 7/02/24.
//

import SwiftUI
import SDWebImageSwiftUI

struct ImageCard: View {
    
    var image: ImageModel
    @Binding var select: ImageModel?
    
    var body: some View {
        
        Button(action: {
            
            select = image
            
        }) {
         
            ZStack {
                
                WebImage(url: URL(string: image.url ?? ""))
                    .resizable()
                    .scaledToFit()
                    .cornerRadius(10)
            }
        }
    }
}

#Preview {
    ImageCard(image: ImageModel(), select: .constant(nil))
}
